package com.jxpanda.r2dbc.spring.data.config.dialect;

import com.jxpanda.r2dbc.spring.data.core.enhance.handler.R2dbcCustomTypeHandlers;
import com.jxpanda.r2dbc.spring.data.core.enhance.handler.R2dbcJsonTypeHandler;
import com.jxpanda.r2dbc.spring.data.core.enhance.handler.R2dbcPostgresJsonTypeHandler;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn("r2dbcCustomTypeHandlers")
@ConditionalOnClass(name = "io.r2dbc.postgresql.codec.Json")
@RequiredArgsConstructor
public class PostgresJsonTypeHandlerInitializer {

    private final R2dbcCustomTypeHandlers r2dbcCustomTypeHandlers;

    @PostConstruct
    public void init() {
        r2dbcCustomTypeHandlers.register(R2dbcJsonTypeHandler.class, new R2dbcPostgresJsonTypeHandler<>());
    }
}
