package demo.api.pg;

import com.jxpanda.r2dbc.spring.data.core.ReactiveEntityTemplate;
import demo.model.pg.LlmPrompt;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @author Panda
 */
@RestController
@RequestMapping("llm")
@RequiredArgsConstructor
public class LlmController implements Controller<LlmPrompt> {

    private final ReactiveEntityTemplate reactiveEntityTemplate;

    @Override
    public ReactiveEntityTemplate reactiveEntityTemplate() {
        return reactiveEntityTemplate;
    }

    @GetMapping("test")
    public Mono<Object> test() {
//        List<Long> ids = List.of(3239241557395636225L, 3239266354066358273L);
//        return reactiveEntityTemplate.select(LlmPrompt.class)
//                .matching(Query.query(EnhancedCriteria.where("role::text").is(LlmPrompt.Role.SYSTEM.name()).and("is_enable").is(true)))
//                .all()
//                .collectList();
        return reactiveEntityTemplate.save(LlmPrompt.builder()
                .role(LlmPrompt.Role.ASSISTANT)
                .build());
    }

}
